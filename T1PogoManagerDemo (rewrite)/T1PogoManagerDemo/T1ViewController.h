#import <UIKit/UIKit.h>
#import "ColorPickerImageView.h"



@class T1PogoManager;
@class T1PogoPen;
@class T1DemoDrawingController;
@class T1PogoNetClient;
@class PaintingBrush;
@class T1PogoEvent;


@interface T1ViewController : UIViewController <ColorPickerDelegate>
{
	T1PogoManager *pogoManager;
	T1DemoDrawingController *drawingController;
	UILabel *debugLabel;
	T1PogoNetClient *netClient;
	PaintingBrush *colorPickerDisplay;
	T1PogoPen *newlyDiscoveredPen;
	UIImageView *penDisconnected;
	UIImageView *penConnected;
	UIImageView *penTipDown;
	UIImageView *penButtonDown;
}

@property (strong) UIGestureRecognizer *examplePinchGesture;
@property (strong) UIGestureRecognizer *exampleSwipeGesture;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *penswitch;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *handswitch;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *warnlabel;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *overonefinger;


-(IBAction)buttonOnePressed:(id)sender;
//-(void)touchesBegan:(NSSet *)touches withEvent:(T1PogoEvent *)event;
- (IBAction)penswitchbutton:(id)sender;
- (IBAction)handswitchbutton:(id)sender;

@end
