//
//  PaintingBrush.m
//  T1PogoManagerDemo
//
//  Created by Peter Skinner on 12/13/11.
//  Copyright (c) 2011 Ten One Design. All rights reserved.
//

#import "PaintingBrush.h"
#import <QuartzCore/QuartzCore.h>

@implementation PaintingBrush

@synthesize size;
@synthesize location;
@synthesize circleStrokeSize;
@synthesize circleStrokeColor;


- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		self.backgroundColor = [UIColor clearColor];    // only needed in uiview/iphone
		self.circleStrokeSize = 2;
		self.circleStrokeColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:.8];
		self.userInteractionEnabled = NO;
		self.layer.shadowColor = [[UIColor whiteColor] CGColor];
		self.layer.masksToBounds = NO;
	}
	return self;
}


- (void)drawRect:(CGRect)rect {
	CGContextRef aContext = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(aContext, [circleStrokeColor CGColor]);
	CGContextSetRGBFillColor(aContext, 1, 1, 1, .05);
	CGContextSetLineWidth(aContext, circleStrokeSize);
	CGContextAddArc(aContext, self.location.x, self.location.y, self.size, 0, 6.2832, 0);
	CGContextStrokePath(aContext);
	CGContextFillPath(aContext);
	self.layer.shadowColor = [circleStrokeColor CGColor];
    //	NSLog(@"drawingWithSize: %f",self.size);
}

@end
