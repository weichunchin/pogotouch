#import "T1DrawingView.h"
#import "T1DrawingPoint.h"



@implementation T1DrawingView



- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		self.isCursor              = NO;
		self.alwaysDraw            = NO;
		self.strokeSmoothingAmount = 3.5;
		self.tag                   = 99; //uninitialized
		self.backgroundColor       = [UIColor clearColor];      // only needed in uiview/iphone
		//		NSLog(@"subview initted");
		self.drawSmoothCurve  = YES;
		self.autoresizingMask = UIViewAutoresizingNone;
		//		[self setMultipleTouchEnabled:YES];	// maybe another time
        
        
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(colorChanged:) name:@"colorChanged" object:nil];
        
		self.strokeColor = [UIColor blackColor];
		[self initContext:frame.size];
	}
	return self;
}

- (void)initContext:(CGSize)contextSize {
	int bitmapBytesPerRow;
    
	// retina support
	CGFloat scale = [[UIScreen mainScreen] scale];
    
	// Declare the number of bytes per row. Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha.
	bitmapBytesPerRow = (contextSize.width * 4 * scale);
	self.cacheContext = CGBitmapContextCreate(NULL, contextSize.width * scale, contextSize.height * scale, 8, bitmapBytesPerRow, CGColorSpaceCreateDeviceRGB(), (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
	CGContextScaleCTM(self.cacheContext, scale, scale);
}

- (void)drawSegmentInContext:(CGContextRef)context withOrigin:(CGPoint)origin destination:(CGPoint)destination startingDiameter:(float)startingDiameter endingDiameter:(float)endingDiameter startingControlPoint:(CGPoint)startingControlPoint endingControlPoint:(CGPoint)endingControlPoint {
	CGPoint p1  = origin;
	CGPoint p2  = destination;
	CGPoint cp1 = startingControlPoint;
	CGPoint cp2 = endingControlPoint;
    
	// Point 1 calculations
	float cp1DeltaX     = cp1.x - p1.x;
	float cp1DeltaY     = cp1.y - p1.y;
	float cp1Theta      = atan2f(cp1DeltaY, cp1DeltaX);
	float p1_1Theta     = cp1Theta - M_PI_2;    // subtract 90 degrees from cp1 angle
	float p1_2Theta     = p1_1Theta + M_PI;             // opposite side of the circle
	float strokeRadius1 = startingDiameter / 2;
    
	float p1_1X   = strokeRadius1 * cosf(p1_1Theta);
	float p1_1Y   = strokeRadius1 * sinf(p1_1Theta);
	CGPoint p1_1  = CGPointMake(p1_1X + p1.x, p1_1Y + p1.y);
	CGPoint cp1_1 = CGPointMake(p1_1.x + cp1DeltaX, p1_1.y + cp1DeltaY);
    
	float p1_2X   = strokeRadius1 * cosf(p1_2Theta);
	float p1_2Y   = strokeRadius1 * sinf(p1_2Theta);
	CGPoint p1_2  = CGPointMake(p1_2X + p1.x, p1_2Y + p1.y);
	CGPoint cp1_2 = CGPointMake(p1_2.x + cp1DeltaX, p1_2.y + cp1DeltaY);
    
    
    
	// Point 2 calculations
	float cp2DeltaX     = cp2.x - p2.x;
	float cp2DeltaY     = cp2.y - p2.y;
	float cp2Theta      = atan2f(cp2DeltaY, cp2DeltaX);
	float p2_1Theta     = cp2Theta + M_PI_2;    // add 90 deg (cp2 is the opposite direction of cp1)
	float p2_2Theta     = p2_1Theta + M_PI;             // opposite side of the circle
	float strokeRadius2 = endingDiameter / 2;
    
	float p2_1X   = strokeRadius2 * cosf(p2_1Theta);
	float p2_1Y   = strokeRadius2 * sinf(p2_1Theta);
	CGPoint p2_1  = CGPointMake(p2_1X + p2.x, p2_1Y + p2.y);
	CGPoint cp2_1 = CGPointMake(p2_1.x + cp2DeltaX, p2_1.y + cp2DeltaY);
    
	float p2_2X   = strokeRadius2 * cosf(p2_2Theta);
	float p2_2Y   = strokeRadius2 * sinf(p2_2Theta);
	CGPoint p2_2  = CGPointMake(p2_2X + p2.x, p2_2Y + p2.y);
	CGPoint cp2_2 = CGPointMake(p2_2.x + cp2DeltaX, p2_2.y + cp2DeltaY);
    
	// draw first curve from outside of point 1 to outside of point 2
	CGContextBeginPath(context);
	CGContextMoveToPoint(context, p1_1.x, p1_1.y);
	CGContextAddCurveToPoint(context, cp1_1.x, cp1_1.y, cp2_1.x, cp2_1.y, p2_1.x, p2_1.y);
    
	// add an arc from outside of point 2 to inside of point 2
	CGContextAddArc(context, p2.x, p2.y, strokeRadius2, p2_1Theta, p2_2Theta, 0);
    
	// draw curve back from inside of point 2 to inside of point 1
	CGContextAddCurveToPoint(context, cp2_2.x, cp2_2.y, cp1_2.x, cp1_2.y, p1_2.x, p1_2.y);
    
    
	// add an arc from inside of point 1 to outside of point 1
	CGContextAddArc(context, p1.x, p1.y, strokeRadius1, p1_2Theta, p1_1Theta, 0);
    
	CGContextClosePath(context);
	CGContextFillPath(context);
    
	// stroke and fill for debug
    //	CGContextSetRGBStrokeColor(context, 0, 1, 0, 1);
    //	CGContextSetLineWidth(context, .5);
    //	CGContextDrawPath(context,kCGPathFillStroke);
}

- (void)drawToCacheWithFinalSegment:(BOOL)drawFinalSegment fullRedraw:(BOOL)fullRedraw {
	//	NSLog(@"drawToCacheWithFinalSegment: %d fullRedraw: %d",drawFinalSegment,fullRedraw);
    
	if (fullRedraw == YES) {
		// clear context before drawing everything again
		CGContextClearRect(self.cacheContext, self.bounds);
	}
    
    
	NSUInteger strokeIndex = 0;
	for (NSMutableArray *myBrushPoints in self.myStrokes) {
		// skip everything but the last stroke unless we're redrawing everything
		// this makes it fast
		if (fullRedraw == NO && strokeIndex < ([self.myStrokes count] - 1)) {
			strokeIndex++;
			continue;
		}
        
		// set up contexty things
		CGContextRef aContext = self.cacheContext;
		CGContextSetStrokeColorWithColor(aContext, [self.strokeColor CGColor]);
		CGContextSetFillColorWithColor(aContext, [self.strokeColor CGColor]);
		CGContextSetLineWidth(aContext, 2);
		CGContextSetLineJoin(aContext, kCGLineJoinRound);
		CGContextSetLineCap(aContext, kCGLineCapRound);
		//		CGContextSetAllowsAntialiasing(aContext,NO);
		T1DrawingPoint *myPoint = [myBrushPoints objectAtIndex:0];
		CGContextMoveToPoint(aContext, myPoint.location.x, myPoint.location.y);
		NSUInteger index = 0;
		NSUInteger count = [myBrushPoints count];
        
		for (T1DrawingPoint *myPoint in myBrushPoints) {
			// skip earlier parts of the stroke if not a full redraw.  This makes it fast.
			if (fullRedraw == NO) {
				if (count > 2 && index < (count - 2)) {
					index++;
					continue;
				}
			}
            
            
			if (index == 0) {                              // draw first dot
				CGContextBeginPath(aContext);
				CGContextMoveToPoint(aContext, myPoint.location.x, myPoint.location.y);
				CGContextSetLineWidth(aContext, myPoint.diameter);
				CGContextAddLineToPoint(aContext, myPoint.location.x, myPoint.location.y);
				CGContextStrokePath(aContext);
			}
            
			// We don't draw when index == 1
            
			else if (index >= 2) {
				// draw filled path with smooth diameter changes
				// strategy:
				// 0) draw a single smoothed curve between points, generating control points cp1, cp2.
				// 1) find two points on stroke diameter normal to control point lines.  Call them p1_1 and p1_2.
				// 2) connect them to p2_1 and p2_2 with curve using the same control point offsets.
				// 3) connect the curves together with an arc equal in diameter to the stroke at that point
				// 4) fill the result.
                
                
				// smoothed curve is based on:  http://www.mvps.org/directx/articles/catmull/
				// and: http://stackoverflow.com/questions/1052119/how-can-i-trace-the-finger-movement-on-touch-for-drawing-smooth-curves
				// example here: http://www.netgraphics.sk/catmull-rom-spline
				// note catmull rom assumes evenly spaced points.  This is a failed assumption, especially on earlier hardware.
				// we assume uneven spacing when calculating control points
                
				CGPoint p0;
				if (index == 2) {
					p0 = CGPointMake(0, 0);
				}
				else {
					p0 = [(T1DrawingPoint *)[myBrushPoints objectAtIndex:(index - 3)] location];
				}
				CGPoint p1 = [(T1DrawingPoint *)[myBrushPoints objectAtIndex:(index - 2)] location];
				CGPoint p2 = [(T1DrawingPoint *)[myBrushPoints objectAtIndex:(index - 1)] location];
				CGPoint p3 = CGPointMake(myPoint.location.x, myPoint.location.y);
				float d02  = [self distanceBetween:p0 and:p2];
				float d12  = [self distanceBetween:p1 and:p2];
				float d13  = [self distanceBetween:p1 and:p3];
				float r1   = (d12 / d02) * (self.strokeSmoothingAmount / 10);                           // 3.33 works as amount
				float r2   = (d12 / d13) * (self.strokeSmoothingAmount / 10);
                
				float cp1x  = p1.x + (p2.x - p0.x) * r1;
				float cp1y  = p1.y + (p2.y - p0.y) * r1;
				float cp2x  = p2.x + (p1.x - p3.x) * r2;
				float cp2y  = p2.y + (p1.y - p3.y) * r2;
				CGPoint cp1 = CGPointMake(cp1x, cp1y);
				CGPoint cp2 = CGPointMake(cp2x, cp2y);
                
                
				float startingDiameter = [[myBrushPoints objectAtIndex:(index - 2)] diameter];
				float endingDiameter   = [[myBrushPoints objectAtIndex:(index - 1)] diameter];
                
                
				if (index == 2) {
					// we don't have cp1, so we'll use cp2 to determine p1_1 and p1_2
					// we'll also trick out cp1 to be a lot shorter than the distance from p1 to cp2
					cp1x = (cp2x - p1.x) / 10 + p1.x;
					cp1y = (cp2y - p1.y) / 10 + p1.y;
					cp1  = CGPointMake(cp1x, cp1y);
				}
                
				[self drawSegmentInContext:aContext withOrigin:p1 destination:p2 startingDiameter:startingDiameter endingDiameter:endingDiameter startingControlPoint:cp1 endingControlPoint:cp2];
                
				// draw again if the stroke is done or if it's a full redraw
				// calculate and draw the last segment that terminates at the last point p3
				if ((drawFinalSegment || fullRedraw) && index == count - 1) {
					// draw that little last bit at the end
					// we don't have cp3, so we'll use the control point after p2 to determine p3_1 and p3_2
                    
					// make control point after p2
					float d23        = [self distanceBetween:p1 and:p3];
					float r3         = (d23 / d13) * (self.strokeSmoothingAmount / 10);
					float cpAfter2x  = p2.x + (p3.x - p1.x) * r3;
					float cpAfter2y  = p2.y + (p3.y - p1.y) * r3;
					CGPoint cpAfter2 = CGPointMake(cpAfter2x, cpAfter2y);
                    
					// shorten it so it looks ok as cp3
					float cp3x  = (cpAfter2x - p3.x) / 10 + p3.x;
					float cp3y  = (cpAfter2y - p3.y) / 10 + p3.y;
					CGPoint cp3 = CGPointMake(cp3x, cp3y);
                    
					startingDiameter = [[myBrushPoints objectAtIndex:(index - 1)] diameter];
					endingDiameter   = [[myBrushPoints objectAtIndex:(index - 0)] diameter];
                    
					[self drawSegmentInContext:aContext withOrigin:p2 destination:p3 startingDiameter:startingDiameter endingDiameter:endingDiameter startingControlPoint:cpAfter2 endingControlPoint:cp3];
				}
                
                
                //				Debug stuff
                //
                //				// show points
                //				CGContextBeginPath(aContext);
                //				CGContextSetRGBFillColor(aContext,.6,.1,.5,1);
                //				CGContextAddArc(aContext, p0.x, p0.y, 4, 0, 6.2832,0);
                //				CGContextFillPath(aContext);
                //
                //				// show points
                //				CGContextBeginPath(aContext);
                //				CGContextSetRGBFillColor(aContext,.6,.1,.5,1);
                //				CGContextAddArc(aContext, p2.x, p2.y, 4, 0, 6.2832,0);
                //				CGContextFillPath(aContext);
                //
                //				// show points
                //				CGContextBeginPath(aContext);
                //				CGContextSetRGBFillColor(aContext,.6,.1,.5,1);
                //				CGContextAddArc(aContext, p3.x, p3.y, 4, 0, 6.2832,0);
                //				CGContextFillPath(aContext);
                //
                //
                //				// reset color
                //				CGContextSetFillColorWithColor(aContext, [self.strokeColor CGColor]);
			}
            
            
			index++;
		}
	}
    
    
    
	// identify a rectangle fully encompasing the last 2 stroke segments that includes stroke diameter + 10
	NSUInteger twoBackIndex = 0;
	if ([[self.myStrokes lastObject] count] > 2) {
		twoBackIndex = [[self.myStrokes lastObject] count] - 3;
	}
	T1DrawingPoint *lastPoint    = [[self.myStrokes lastObject] lastObject];
	T1DrawingPoint *twoBackPoint = [[self.myStrokes lastObject] objectAtIndex:twoBackIndex];
    
	float dirtyOriginX             = MIN(lastPoint.location.x, twoBackPoint.location.x);
	float dirtyOriginY             = MIN(lastPoint.location.y, twoBackPoint.location.y);
	float dirtySizeX               = fabs(lastPoint.location.x - twoBackPoint.location.x);
	float dirtySizeY               = fabs(lastPoint.location.y - twoBackPoint.location.y);
	CGRect segmentRect             = CGRectMake(dirtyOriginX, dirtyOriginY, dirtySizeX, dirtySizeY);
	float strokeRadiusWithExtraBit = MAX(lastPoint.diameter, twoBackPoint.diameter) / 2 + 10;
	CGRect dirtyRect               = CGRectInset(segmentRect, -strokeRadiusWithExtraBit, -strokeRadiusWithExtraBit);
    
	// interesting to note the fast strokes + smoothing algorithm can cause stroke to exceed the dirty rectangle.
	// fixing this might be as easy as expanding the rectangle to include all control points.
	// I'm thinking the drawSegmentInContext: method should return a dirty rectangle.  That would be nice and clean.
    
    
	if (fullRedraw) {
		dirtyRect = self.bounds;
	}
	[self setNeedsDisplayInRect:dirtyRect];
}

- (void)drawRect:(CGRect)rect {
	// backing store draw
	CGContextRef context = UIGraphicsGetCurrentContext();
    
    
	CGImageRef cacheImage = CGBitmapContextCreateImage(self.cacheContext);
	CGContextDrawImage(context, self.bounds, cacheImage);
	CGImageRelease(cacheImage);
}

- (CGFloat)distanceBetween:(CGPoint)point1 and:(CGPoint)point2 {
	CGFloat dx = point2.x - point1.x;
	CGFloat dy = point2.y - point1.y;
	CGFloat d  = sqrt(dx * dx + dy * dy);
	return d ? d : 0.0001;
}

- (void)colorChanged:(NSNotification *)proxNotice {
	//	NSLog(@"strokeColorChanged in HistoryView");
	self.strokeColor = (UIColor *)[[proxNotice userInfo] valueForKey:@"color"];
	[self setNeedsDisplay];
}

- (void)strokeWidthChanged:(NSNotification *)proxNotice {
	self.strokeWidth = [(NSNumber *)[[proxNotice userInfo] valueForKey:@"strokeWidth"] floatValue];
	[self setNeedsDisplay];
}

- (void)dealloc {
	//	NSLog(@"stroke dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
