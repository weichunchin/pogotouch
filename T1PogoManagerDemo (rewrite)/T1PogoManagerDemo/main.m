#import <UIKit/UIKit.h>

#import "T1AppDelegate.h"


int main(int argc, char *argv[])
{
	@autoreleasepool
    {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([T1AppDelegate class]));
	}
}
