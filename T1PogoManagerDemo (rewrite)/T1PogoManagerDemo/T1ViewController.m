#import "T1ViewController.h"
#import "T1PogoManager.h"
#import "T1DemoDrawingController.h"
#import "ColorPickerImageView.h"
#import "PaintingBrush.h"
#import "T1PogoEvent.h"




@implementation T1ViewController

@synthesize penswitch;
@synthesize handswitch;
@synthesize warnlabel;
@synthesize overonefinger;
- (IBAction)penswitchbutton:(id)sender {
    if(penswitch.on) handswitch.on = false ;
    
}
- (IBAction)handswitchbutton:(id)sender {
    if(handswitch.on) penswitch.on = false ;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.view.frame = CGRectMake(0, 0, 1024, 768);
	UIView *drawingView = [[UIView alloc] initWithFrame:CGRectMake(38, 40, 660, 684)];
	drawingView.userInteractionEnabled = NO;            // forces touch events to propogate to this controller for convenience
	[self.view addSubview:drawingView];

	
	pogoManager = [T1PogoManager pogoManagerWithDelegate:self];	// be sure to retain if not using ARC
	[pogoManager registerView:self.view];						// pass a view that is receiving touch events.  In this case it's self.view
	[pogoManager setEnablePenInputOverNetworkIfIncompatiblePad:YES];	// allow pens to connect to iPad 1 & 2.  Highly recommended!
	
	drawingController = [[T1DemoDrawingController alloc] initWithView:drawingView];
	[drawingController setMyController:self];
	NSLog(@"Pogo SDK build number %d", (int)[pogoManager buildNumber]);
	
	self.view.multipleTouchEnabled = YES;
	self.view.backgroundColor      = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
	/*
	penDisconnected       = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pen_image_disconnected"]];
	penConnected          = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pen_image_connected"]];
	penTipDown            = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pen_image_tip_down"]];
	penButtonDown         = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pen_image_button_down"]];
	penDisconnected.frame = CGRectMake(780, 145, penDisconnected.frame.size.width, penDisconnected.frame.size.height);
	penConnected.frame    = CGRectMake(780, 145, penConnected.frame.size.width, penConnected.frame.size.height);
	penTipDown.frame      = CGRectMake(780, 145, penTipDown.frame.size.width, penTipDown.frame.size.height);
	penButtonDown.frame   = CGRectMake(780, 145, penButtonDown.frame.size.width, penButtonDown.frame.size.height);
	[penConnected setHidden:YES];
	[penTipDown setHidden:YES];
	[penButtonDown setHidden:YES];
	[self.view addSubview:penDisconnected];
	[self.view addSubview:penConnected];
	[self.view addSubview:penTipDown];
	[self.view addSubview:penButtonDown];*/
	/*
	UIImage *buttonBackgroundImage = [UIImage imageNamed:@"bluebutton"];
	UIImage *stretchedBackground   = [buttonBackgroundImage stretchableImageWithLeftCapWidth:33 topCapHeight:0];
	
	UIButton *restartButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[restartButton setFrame:CGRectMake(735, 639, 130, 30)];
	[restartButton setTitle:@"Show Pens" forState:UIControlStateNormal];
	[restartButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	restartButton.titleLabel.font = [UIFont fontWithName:@"Chalet-LondonNineteenSeventy" size:14];
	[restartButton addTarget:self action:@selector(showScanningUIButtonAction:) forControlEvents:UIControlEventTouchUpInside];
	[restartButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[restartButton setBackgroundImage:stretchedBackground forState:UIControlStateNormal];
	[self.view addSubview:restartButton];
	
	UIButton *lightShow = [UIButton buttonWithType:UIButtonTypeCustom];
	[lightShow setFrame:CGRectMake(735, 689, 130, 30)];
	[lightShow setTitle:@"Light Show" forState:UIControlStateNormal];
	[lightShow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	lightShow.titleLabel.font = [UIFont fontWithName:@"Chalet-LondonNineteenSeventy" size:14];
	[lightShow addTarget:self action:@selector(lightShowAction:) forControlEvents:UIControlEventTouchUpInside];
	[lightShow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[lightShow setBackgroundImage:stretchedBackground forState:UIControlStateNormal];
	[self.view addSubview:lightShow];
	
	debugLabel = [[UILabel alloc] init];
	[debugLabel setText:@""];
	[debugLabel setFrame:CGRectMake(740, 595, 224, 25)];
	[debugLabel setBackgroundColor:[UIColor clearColor]];
	[debugLabel setTextColor:[UIColor whiteColor]];
	[debugLabel setFont:[UIFont fontWithName:@"Chalet-LondonNineteenSeventy" size:debugLabel.font.pointSize]];
	[self.view addSubview:debugLabel];
	
	UILabel *titleLabel = [[UILabel alloc] init];
	titleLabel.lineBreakMode = UILineBreakModeWordWrap;
	titleLabel.numberOfLines = 0;
	[titleLabel setText:@"Pogo Manager\nDemo Project"];
	[titleLabel setFrame:CGRectMake(704, 0, 320, 180)];
	[titleLabel setBackgroundColor:[UIColor clearColor]];
	[titleLabel setTextColor:[UIColor whiteColor]];
	[titleLabel setFont:[UIFont fontWithName:@"Chalet-LondonNineteenSeventy" size:36]];
	[titleLabel setTextAlignment:NSTextAlignmentCenter];
	[self.view addSubview:titleLabel];*/
	
	ColorPickerImageView *colorWheel = [[ColorPickerImageView alloc] initWithImage:[UIImage imageNamed:@"color_picker"]];
	colorWheel.pickedColorDelegate = self;
	[colorWheel setFrame:CGRectMake(850, 625, colorWheel.frame.size.width, colorWheel.frame.size.height)];
	
/*	colorPickerDisplay = [[PaintingBrush alloc] initWithFrame:self.view.frame];
	[colorPickerDisplay setSize:58];
	[colorPickerDisplay setLocation:CGPointMake(140, 90)];
	[colorPickerDisplay setCircleStrokeColor:[UIColor clearColor]];
	[colorPickerDisplay setCircleStrokeSize:8];
	[colorPickerDisplay setLocation:CGPointMake(947, 675)];*/
/*
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_iphone"]];
		[drawingController setDrawingViewFrame:CGRectMake(12, 13, 295, 323)];
		[colorWheel setFrame:CGRectMake(195, 352, colorWheel.frame.size.width, colorWheel.frame.size.height)];
		[colorPickerDisplay setLocation:CGPointMake(245, 402)];
		[debugLabel setFrame:CGRectMake(0, 455, 320, 25)];
		[debugLabel setTextAlignment:NSTextAlignmentCenter];
		[restartButton setFrame:CGRectMake(25, 370, 130, 30)];
		[lightShow setFrame:CGRectMake(25, 410, 130, 30)];
	}*/
	
	[self.view addSubview:colorWheel];
	[self.view addSubview:colorPickerDisplay];
	
	self.examplePinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
	self.exampleSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
	self.examplePinchGesture.cancelsTouchesInView = NO;
	self.exampleSwipeGesture.cancelsTouchesInView = NO;
	[self.view addGestureRecognizer:self.examplePinchGesture];
	[self.view addGestureRecognizer:self.exampleSwipeGesture];
}

#pragma mark - Buttons



- (void)showScanningUIButtonAction:(id)sender {
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
		UIPopoverController *popover = [pogoManager scanningPopover];
		[popover presentPopoverFromRect:[sender frame] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
	}
	else {
		[self presentViewController:[pogoManager scanningViewControllerForPhone] animated:YES completion:nil];
	}
}

- (void)lightShowAction:(id)sender {
	[debugLabel setText:@"Light Show"];
	float msStepLength = 400;
	float hue;
	for (int i = 1; i < 15; i++) {
		hue = ((double)arc4random() / 0x100000000);             // random number between 0 and 1
		UIColor *color = [UIColor colorWithHue:hue saturation:1.0f brightness:1.0f alpha:1.0f];
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, msStepLength * i * NSEC_PER_MSEC), dispatch_get_current_queue(), ^{
		    [pogoManager fadeToLEDColor:color overTime:0.15f forDuration:1.0f];
		});
	}
}


#pragma mark - Touch Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([pogoManager touchIsPen:touch]) {
            if(penswitch.on == YES){
			[drawingController drawTouchBeganWithPoint:[touch locationInView:drawingController.drawingView] pressure:[pogoManager pressureForTouch:touch] timestamp:touch.timestamp];
                NSLog(@"hi");}
            //[drawingController setColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
		}
        else{
            if(handswitch.on == YES && overonefinger.on == NO){
                overonefinger.on = YES;
            [drawingController drawTouchBeganWithPoint:[touch locationInView:drawingController.drawingView] pressure:[pogoManager pressureForTouch:touch] timestamp:touch.timestamp];
                NSLog(@"NO");}
           // [drawingController setColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
        }
        
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		// touchIsPen: method clocked at about 15us
		if ([pogoManager touchIsPen:touch]) {
            if(penswitch.on == YES){
                NSLog(@"hi");
                [drawingController drawTouchMovedWithPoint:[touch locationInView:drawingController.drawingView] pressure:[pogoManager pressureForTouch:touch] timestamp:touch.timestamp];}
		}
        else{
            if(handswitch.on == YES){
            [drawingController drawTouchMovedWithPoint:[touch locationInView:drawingController.drawingView] pressure:[pogoManager pressureForTouch:touch] timestamp:touch.timestamp];
            }}
        
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	for (UITouch *touch in touches) {
		if ([pogoManager touchIsPen:touch]) {
            if(penswitch.on == YES){
                NSLog(@"hi");
                [drawingController drawTouchEndedWithTimestamp:[event timestamp]];}
		}
        else{
            if(handswitch.on == YES){
            [drawingController drawTouchMovedWithPoint:[touch locationInView:drawingController.drawingView] pressure:[pogoManager pressureForTouch:touch] timestamp:touch.timestamp];
                overonefinger.on = NO;
            }
        }
        
	}
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}

#pragma mark - Example Gesture Handling



- (IBAction)handlePinchGesture:(UIPanGestureRecognizer *)sender {
	    NSLog(@"pinch");
    warnlabel.text=@"warning!!!";
    [drawingController undoDrawingStroke];
}

- (IBAction)handleSwipeGesture:(UISwipeGestureRecognizer *)sender{
	    NSLog(@"swipe");
}

#pragma mark - T1PogoManager Delegate Methods

- (void)pogoManager:(T1PogoManager *)manager didUpdateBluetoothState: (CBCentralManagerState)state
{
	if (state == CBCentralManagerStatePoweredOff) {
		NSLog(@"Bluetooth is powered off.  You could choose to tell your user.");
	}
}

// By default, T1PogoManager will prompt the user to connect any new pen it discovers.
// If you'd like to customize this behavior, set showConnectionPromptForNewPens to NO, and implement this method.
// This will fire once per app launch if an unknown new pen is discovered.  Delete/reinstall the app to reset the state.
// If you ultimately decide to connect a pen, keep a temporary reference to it around, then call connectPogoPen:
- (void)pogoManager:(T1PogoManager *)manager didDiscoverNewPen:(T1PogoPen *)pen withName:(NSString *)name {
}

- (void)pogoManager:(T1PogoManager *)manager willConnectPen:(T1PogoPen *)pen {
	NSLog(@"will connect pen (app): %@", pen.peripheral.productName);
}

- (void)pogoManager:(T1PogoManager *)manager didConnectPen:(T1PogoPen *)pen {
	NSLog(@"did connect pen (app): %@", pen.peripheral.productName);
	[debugLabel setText:pen.peripheral.productName];
	[penConnected setHidden:NO];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Bluetooth connect"
                                                       message: @"Your pen has connected"
                                                      delegate: self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        
    [alert setTag:1];
    [alert show];
	
	// this is a great place to choose the desired pressure response, linear, light, or heavy.
	// it defaults to linear.  This is just here as an example.
	// consider if T1PogoPenPressureResponseLight is best for artistic apps.
	[manager setPressureResponse:T1PogoPenPressureResponseLinear forPen:pen];
	
	
	// Here's an example of reading and writing data to pen using block-based API
	// reads and writes are serialized and queued.
	// The SDK will wait for this write to complete before processing the read.
	NSData *testData = [NSNetService dataFromTXTRecordDictionary:@{ @"example": @"dictionary" }];
	[manager writeApplicationData:testData onPeripheral:[pen peripheral] completionHandler:nil];
	[manager readApplicationDataOnPeripheral:[pen peripheral] completionHandler: ^(NSData *data, NSError *error)
	 {
		 if (!error) {
			 NSDictionary *dictionary = [NSNetService dictionaryFromTXTRecordData:data];
			 NSString *key = @"example";
			 NSString *value = [NSString stringWithUTF8String:[[dictionary objectForKey:key] bytes]];
			 NSLog(@"Pen storage demo result: @{@\"%@\":@\"%@\"}", key, value);
		 }
	 }];
}

- (void)pogoManager:(T1PogoManager *)manager didDisconnectPen:(T1PogoPen *)pen {
	NSLog(@"did disconnect pen");
	[debugLabel setText:@"Pen has disconnected"];
	[penConnected setHidden:YES];
	[penTipDown setHidden:YES];
	[penButtonDown setHidden:YES];
}

- (void)pogoManager:(T1PogoManager *)manager didUpdatePen:(T1PogoPen *)pen {
	NSLog(@"Pen Update: %@", pen.peripheral.manufacturerName);
	[debugLabel setText:[NSString stringWithFormat:@"%@ - %@", pen.peripheral.manufacturerName, pen.peripheral.productName]];
}

- (void)pogoManager:(T1PogoManager *)manager didChangePressureWithoutMoving:(T1PogoEvent *)event forPen:(T1PogoPen *)pen {
	//	pressureVal: %f",[pen lastPressure]);
    if(penswitch.on==YES){
    NSLog(@"pressure");
        [drawingController drawTouchChangedPressure:event.pressure timestamp:event.firstTimestamp];}
}



- (void)pogoManager:(T1PogoManager *)manager didChangeTouchType:(T1PogoEvent *)event forPen:(T1PogoPen *)pen{
    
    if (event.touchType == T1TouchTypePen1){
        NSLog(@"pen");
    }
    if (event.touchType == T1TouchTypeUnknown){
        NSLog(@"unknown");
    }
    if (event.touchType == T1TouchTypeFinger){
        NSLog(@"finger");
    }
    if (event.previousTouchType == T1TouchTypePen1){
        NSLog(@"ptouch is a pen");
    }
    if (event.previousTouchType == T1TouchTypeUnknown){
        NSLog(@"ptouch is unknown");
    }
    if (event.previousTouchType == T1TouchTypeFinger){
        NSLog(@"ptouch is finger");
    }
	
	if (event.touchType == T1TouchTypePen1 && event.previousTouchType == T1TouchTypeUnknown) {
		NSLog(@"Unknown touch becomes a Pen -- Start drawing");
        if(penswitch.on == YES){
		// this drawing command is important because if this is a quick tap, no further messages may be received, so we should draw now.
            NSLog(@"test");
            [drawingController drawTouchMovedWithPoint:[event.touch locationInView:drawingController.drawingView] pressure:event.pressure timestamp:event.timestamp];}
        //[drawingController setColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
	}
	
	if (event.touchType == T1TouchTypePen1 && event.previousTouchType == T1TouchTypeFinger) {
		NSLog(@"Finger becomes a Pen -- Start drawing");
        if(penswitch.on == YES){
		// start drawing with this touch.  it looked like a finger before, but now we're sure it's a pen
            NSLog(@"test");
            [drawingController drawTouchMovedWithPoint:[event.touch locationInView:drawingController.drawingView] pressure:event.pressure timestamp:event.timestamp];}
        
	}
	
	if (event.touchType == T1TouchTypeFinger && event.previousTouchType == T1TouchTypePen1) {
		// stop drawing with this touch and undo the stroke.  it is definitely not a pen.
		NSLog(@"Pen becomes a Finger -- Undo stroke");
        [drawingController drawTouchMovedWithPoint:[event.touch locationInView:drawingController.drawingView] pressure:event.pressure timestamp:event.timestamp];
        //[drawingController setColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
	}
	if (event.touchType == T1TouchTypeUnknown && event.previousTouchType == T1TouchTypePen1) {
		// stop drawing with this touch and undo the stroke.  it is definitely not a pen.
		NSLog(@"Pen becomes Unknown -- Undo stroke");
		[drawingController undoDrawingStroke];
	}
}

- (void)pogoManager:(T1PogoManager *)manager didDetectButtonDown:(T1PogoEvent *)event forPen:(T1PogoPen *)pen {
	NSLog(@"button %d down", (int)event.button);
	[debugLabel setText:[NSString stringWithFormat:@"button %d is down", (int)event.button]];
	[penButtonDown setHidden:NO];
	[drawingController resetCanvas];
}

- (void)pogoManager:(T1PogoManager *)manager didDetectButtonUp:(T1PogoEvent *)event forPen:(T1PogoPen *)pen {
	NSLog(@"button %d up", (int)event.button);
	[debugLabel setText:[NSString stringWithFormat:@"button %d is up", (int)event.button]];
	[penButtonDown setHidden:YES];
}

- (void)pogoManager:(T1PogoManager *)manager didDetectTipDown:(T1PogoEvent *)event forPen:(T1PogoPen *)pen {
//	[drawingController setColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1]];
    NSLog(@"tip down");
	[debugLabel setText:[NSString stringWithFormat:@"tip is down"]];
	[penTipDown setHidden:NO];
    
}

- (void)pogoManager:(T1PogoManager *)manager didDetectTipUp:(T1PogoEvent *)event forPen:(T1PogoPen *)pen;
{
	NSLog(@"tip up");
	[debugLabel setText:[NSString stringWithFormat:@"tip is up"]];
	[penTipDown setHidden:YES];
    
}



- (void)pogoManager:(T1PogoManager *)manager didChangeDebugString:(NSString *)string {
	[debugLabel setText:string];
}

- (void)pogoManager:(T1PogoManager *)manager didDetectLowBatteryForPen:(T1PogoPen *)pen {
	NSLog(@"Low Battery - replace soon.");
}

- (void)pogoManagerDidSuggestDisablingGesturesForRegisteredViews:(T1PogoManager *)manager {
	NSLog(@"-- DISABLE NAVIGATION GESTURES --");
	self.exampleSwipeGesture.enabled = NO;
	self.examplePinchGesture.enabled = NO;
}

- (void)pogoManagerDidSuggestEnablingGesturesForRegisteredViews:(T1PogoManager *)manager {
	NSLog(@"-- ENABLE NAVIGATION GESTURES --");
	self.exampleSwipeGesture.enabled = YES;
	self.examplePinchGesture.enabled = YES;
}

// Color Picker Delegate Methods
- (void)hoverColor:(UIColor *)color {
	[colorPickerDisplay setCircleStrokeColor:color];
	[colorPickerDisplay setNeedsDisplay];
}

- (void)pickedColor:(UIColor *)color {
	if (CGColorGetAlpha([color CGColor]) < .1) {        // if transparent area is selected, make it white
		color = [UIColor whiteColor];
	}
	[colorPickerDisplay setCircleStrokeColor:[UIColor clearColor]];
	[colorPickerDisplay setNeedsDisplay];
	[drawingController setColor:color];
	[pogoManager setLEDColor:color duration:5];
}

-(IBAction)buttonOnePressed:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Clear all"
                                                   message: @"Are you sure"
                                                  delegate: self
                                         cancelButtonTitle:@"No"
                                         otherButtonTitles:@"Yes",nil];
    
    [alert setTag:1];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) { // UIAlertView with tag 1 detected
        if (buttonIndex == 0)
        {
            NSLog(@"user pressed Button Indexed 0");
            // Any action can be performed here
        }
        else
        {
            NSLog(@"user pressed Button Indexed 1");
            [drawingController resetCanvas];
        }
    }}

#pragma mark - View lifecycle

- (UIStatusBarStyle)preferredStatusBarStyle {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000 // iOS 7.0 or later
	return UIStatusBarStyleLightContent;
#else
	return UIStatusBarStyleBlackTranslucent;
#endif
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		return (interfaceOrientation == UIInterfaceOrientationPortrait);
	}
	else {
		return ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation == UIInterfaceOrientationLandscapeRight));
	}
}

@end
