//
//  T1AppDelegate.h
//  T1PogoManagerDemo
//
//  Created by Peter Skinner on 11/2/11.
//  Copyright (c) 2011 Ten One Design. All rights reserved.
//

#import <UIKit/UIKit.h>
@class T1ViewController;

@interface T1AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) T1ViewController *viewController;

@end
