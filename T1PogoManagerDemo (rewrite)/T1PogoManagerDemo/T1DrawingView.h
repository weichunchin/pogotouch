//
//  T1DrawingView.h
//  T1PogoManagerTest
//
//  Created by Peter Skinner on 2/18/13.
//  Copyright (c) 2013 Ten One Design LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface T1DrawingView : UIView

@property (assign) NSMutableArray *myStrokes;

@property (assign) int cursorType;
@property (assign) CGPoint cursorLocation;
@property (assign) BOOL isCursor;
@property (assign) BOOL alwaysDraw;
@property (assign) float strokeSmoothingAmount;
@property (assign) BOOL drawSmoothCurve;
@property (assign) CGRect strokeBounds;
@property (nonatomic, assign) NSInteger tag;
@property (strong)      UIColor *strokeColor;
@property (assign) float strokeWidth;
@property (assign) CGContextRef cacheContext;

- (void)initContext:(CGSize)contextSize;
- (void)drawToCacheWithFinalSegment:(BOOL)drawFinalSegment fullRedraw:(BOOL)fullRedraw;


@end
