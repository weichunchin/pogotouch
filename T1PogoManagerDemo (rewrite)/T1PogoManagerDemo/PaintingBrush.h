//
//  PaintingBrush.h
//  T1PogoManagerDemo
//
//  Created by Peter Skinner on 12/13/11.
//  Copyright (c) 2011 Ten One Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaintingBrush : UIView {
}

@property float size;
@property CGPoint location;
@property float circleStrokeSize;
@property (retain) UIColor *circleStrokeColor;
@end
